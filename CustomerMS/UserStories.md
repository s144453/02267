# User Stories
## Merchant
__As a merchant__  
__I want to:__ Scan a customers barcode  
__So that:__ A payment can be authorized and proven  
__Priority:__  
__Estimate:__  

__As a merchant__  
__I want to:__ Have a DTUpay account  
__So that:__ I can manage my information  
__Priority:__  
__Estimate:__  

__As a merchant__  
__I want to:__ See my transactions  
__So that:__ I can see each transaction, how much money was transfered and which unique token (barcode) was used to authorize the transaction for a given time period. (Merchant should not know who the customer was to keep the payment system semi-anonymous)  
__Priority:__  
__Estimate:__  

__As a merchant__  
__I want to:__ Refund a payment  
__So that:__ I can return money to the customer if needed  
__Priority:__  
__Estimate:__ 

## Customer
__As a customer__  
__I want to:__ Have six unique tokens (barcodes)  
__So that:__ I can authorize purchases from merchants  
__Priority:__  
__Estimate:__  

__As a customer__  
__I want to:__ Have a DTUpay account  
__So that:__ I can manage my information  
__Priority:__  
__Estimate:__  

__As a customer__  
__I want to:__ Obtain more barcodes  
__So that:__ I can authorize more purchases from merchants  
__Priority:__  
__Estimate:__  

__As a customer__  
__I want to:__ Pay merchants  
__So that:__ I can get goods  
__Priority:__  
__Estimate:__  

__As a customer__  
__I want to:__ See my transactions on DTUpay  
__So that:__ I see how much money i paid to which merchant and which unique token (barcode) was used to authorize the transaction in a given time period  
__Priority:__  
__Estimate:__  
