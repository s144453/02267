# 02267 - Development of webservices
## Table of contents
[DTUPay](#dtupay)  
&nbsp;&nbsp; [Jenkins common user](#jenkins-common-user)  
&nbsp;&nbsp; [MySQL](#mysql)  
&nbsp;&nbsp; [RabbitMQ user](#rabbitmq-user)  
[Installation](#installation)  
&nbsp;&nbsp; [Prerequisites](#prerequisites)  
&nbsp;&nbsp; [Installation](#installation)  
&nbsp;&nbsp; [Running the tests](#running-the-tests)  
&nbsp;&nbsp; [Authors](#authors)  

## DTUPay
DTUPay is based on a mobile payment option for shop owners and customers.  
This project contains the source code for the DTUPay service.

### Jenkins common user
* Username: common
* Password: common123

### MySql
* Username: dev
* Password: jakarta
* Database name: Task5

### RabbitMQ user
* Username: MsgQueue
* Password: admin

## Installation
### Prerequisites
### Installing
1. Clone this repo to your local machine using https://gitlab.gbar.dtu.dk/s144453/02267.git
2. Install required war dependencies with maven

### Running the tests
1. Run the RestService package
2. Navigate to the thorntail jar file in the target folder of RestService and run it
3. Run the cucumber tests

### Authors
* Kasper Sørensen
* Mads Maibohm
* Dimitri Khlebutin
* Sammy Masoule
* Jesper Bang