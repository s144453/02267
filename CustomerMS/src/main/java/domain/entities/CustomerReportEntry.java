package domain.entities;

/**
 * @author Dimitri
 */

public class CustomerReportEntry extends ReportEntry {

    private String merchantId;

    public CustomerReportEntry(Payment payment){
        super(payment);
        this.merchantId = payment.getMerchantId();
    }
}
