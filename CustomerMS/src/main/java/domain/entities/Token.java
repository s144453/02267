package domain.entities;

import org.krysalis.barcode4j.impl.code128.Code128Bean;
import org.krysalis.barcode4j.impl.code128.Code128Constants;
import org.krysalis.barcode4j.output.bitmap.BitmapCanvasProvider;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.concurrent.ThreadLocalRandom;

/**
 * @author Kasper
 */

public class Token {

    private String tokenId;
    private BufferedImage barcode;
    private Customer customer;
    private boolean used;

    public Token() {
        this.tokenId = generateTokenId();
        this.barcode = generateBarCode(this.tokenId);
        this.customer = null;
        this.used = false;
    }

    public Token(String tokenId) {
        this.tokenId = tokenId;
        this.barcode = generateBarCode(tokenId);
        this.customer = null;
        this.used = false;
    }

    private String generateTokenId(){
        StringBuilder tokenId = new StringBuilder();
        for (int i = 0; i < 12; i++) {
            tokenId.append(ThreadLocalRandom.current().nextInt(0, 10));
        }
        return tokenId.toString();
    }


    private BufferedImage generateBarCode(String barcodeId) {
        Code128Bean bean = new Code128Bean();
        bean.setCodeset(Code128Constants.CODESET_C);

        final int dpi = 200;

        try {

            OutputStream out = new FileOutputStream(new File("barcode.png"));

            BitmapCanvasProvider provider = new BitmapCanvasProvider(out, "image/x-png", dpi,
                    BufferedImage.TYPE_BYTE_GRAY, false, 0);

            bean.generateBarcode(provider, barcodeId);
            provider.finish();
            out.close();

            return provider.getBufferedImage();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String getTokenId() {
        return tokenId;
    }

    public BufferedImage getBarcode() {
        return barcode;
    }

    public Customer getCustomer() {
        return customer;
    }

    public void setCustomer(Customer customer) {
        this.customer = customer;
    }

    public boolean isUsed() {
        return used;
    }

    public void use(){ this.used = true; }

}
