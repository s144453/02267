package domain.entities;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

/**
 * @author Kasper
 */

public class Payment {

    private String paymentId;
    private BigDecimal amount;
    private Token token;
    private Customer customer;
    private String merchantId;
    private LocalDateTime time;
    private boolean refunded;
    private String description;

    public Payment(BigDecimal amount, Token token, Customer customer, String merchantId) {
        this.paymentId = UUID.randomUUID().toString();
        this.amount = amount;
        this.token = token;
        this.customer = customer;
        this.merchantId = merchantId;
        this.time = LocalDateTime.now();
        this.refunded = false;
        this.description = "";
    }

    public Payment(BigDecimal amount, Token token, Customer customer, String merchantId, LocalDateTime time) {
        this.paymentId = UUID.randomUUID().toString();
        this.amount = amount;
        this.token = token;
        this.customer = customer;
        this.merchantId = merchantId;
        this.time = time;
        this.refunded = false;
        this.description = "";
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Token getToken() {
        return token;
    }

    public String getMerchantId() {
        return this.merchantId;
    }

    public String getDescription() {
        return this.description;
    }

    public LocalDateTime getTime() { return this.time; }

    public LocalDateTime getDate() { return this.time.truncatedTo(ChronoUnit.DAYS); }

    public Customer getCustomer() { return this.customer; }

    public void setTime(LocalDateTime time) {this.time = time; }

    public void setRefund(boolean isRefunded) { this.refunded = isRefunded; }
}
