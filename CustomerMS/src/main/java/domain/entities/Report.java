package domain.entities;

import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * @author Dimitri
 */

public class Report {

    private String reportId;
    List<ReportEntry> entryList;

    public Report(List<ReportEntry> entryList) {
        this.reportId = entryList.get(0).getTime().format(DateTimeFormatter.ofPattern("MM-yyyy"));
        this.entryList = entryList;
    }

    public String getReportId() { return this.reportId; }

}
