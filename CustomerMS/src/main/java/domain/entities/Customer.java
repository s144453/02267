package domain.entities;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Kasper
 */

public class Customer extends User {

    private List<Token> tokens;

    public Customer(String firstName, String lastName){
        super(firstName, lastName);
        this.tokens = new ArrayList<>();
    }

    public List<Token> getTokens() {
        return tokens;
    }

    public void addToken(Token token){
        tokens.add(token);
    }

}
