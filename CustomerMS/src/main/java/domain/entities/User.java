package domain.entities;

import java.util.UUID;

/**
 * @author Kasper
 */

public abstract class User {

    private String uniqueId;
    private String firstName;
    private String lastName;

    public User(String firstName, String lastName) {
        this.uniqueId = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getName() {
        return firstName + " " + lastName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
