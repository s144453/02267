package domain.entities;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Dimitri
 */

public abstract class ReportEntry {

    private BigDecimal amount;
    private Token token;
    private String description;
    private LocalDateTime time;

    public ReportEntry(Payment payment) {
        this.amount = payment.getAmount();
        this.token = payment.getToken();
        this.description = payment.getDescription();
        this.time = payment.getTime();
    }

    public Token getToken() { return token; }

    public LocalDateTime getTime() { return time; }
}
