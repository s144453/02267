package domain.services;

import domain.entities.Customer;
import domain.entities.Token;

import java.util.List;
import java.util.Optional;

/**
 * @author Kasper
 */

public interface TokenService {

    byte[] getTokenImage(String tokenId);

    List<Token> getTokensOfCustomer(Customer customer);

    boolean useToken(String id);

    Optional<Token> getTokenById(String id);

    List<Token> getUnusedTokensByID(String UUID);

    boolean requestTokenByID(String UUID, int n);


}
