package domain.services;

import domain.entities.Report;
import dtu.ws.fastmoney.BankServiceException_Exception;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @author Jesper
 */

public interface CustomerService {

    String createCustomer(String firstName, String lastName, BigDecimal balance);

    List<Report> getListOfReports(String customerId);

    String getReport(String cId, String rId);

    String changeCustomerName(String customerId, String newFirstName, String newLastName);

    boolean deleteCustomer(String id) throws BankServiceException_Exception;

    boolean createCustomerPayment(BigDecimal amount, String tokenId, String merchantId, LocalDateTime time);

    void refundCustomerPayment(String tokenId);

}
