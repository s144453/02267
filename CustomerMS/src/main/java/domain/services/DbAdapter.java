package domain.services;

import domain.entities.*;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * @author Kasper
 */

public interface DbAdapter {

    Optional<Customer> getCustomer(String id);

    Optional<Token> getToken(String tokenId);

    List<Token> getTokens();

    List<Payment> getPayments();

    Customer createCustomer(String firstName, String lastName);

    void createToken(Customer customer);

    Payment createPayment(BigDecimal amount, Token token, Customer customer, String merchantId);

    void updateName(Customer customer, String newFirstName, String newLastName);

}
