package domain.services;

import domain.entities.Customer;
import domain.entities.Token;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Kasper
 * @co-author Sammy
 */

public class TokenServiceImpl implements TokenService {


    private static final int TOKEN_MAX = 5;
    private static DbAdapter dbAdapter = InMemoryDbImpl.getInstance();

    public TokenServiceImpl() {
    }


    @Override
    public byte[] getTokenImage(String tokenId) {
        Optional<Token> optToken = dbAdapter.getToken(tokenId);
        if (!optToken.isPresent()) return null;
        BufferedImage bi = optToken.get().getBarcode();

        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        try {
            ImageIO.write(bi, "png", baos);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return baos.toByteArray();
    }


    @Override
    public boolean useToken(String tokenId) {
        Optional<Token> token = dbAdapter.getToken(tokenId);
        if (token.isPresent()) {
            if (!token.get().isUsed()) {
                token.get().use();
                return true;
            }
        }
        return false;
    }


    @Override
    public Optional<Token> getTokenById(String id) {
        return dbAdapter.getToken(id);
    }


    @Override
    public List<Token> getUnusedTokensByID(String UUID) {
        Optional<Customer> customer = dbAdapter.getCustomer(UUID);
        return customer.map(c -> getTokensOfCustomer(c)
                .stream()
                .filter(t -> !t.isUsed())
                .collect(Collectors.toList())).orElse(null);
    }

    public List<Token> getTokensOfCustomer(Customer customer) {
        return dbAdapter.getTokens().stream()
                .filter(t -> t.getCustomer().equals(customer))
                .collect(Collectors.toList());
    }

    @Override
    public boolean requestTokenByID(String UUID, int n) {
        Optional<Customer> customer = dbAdapter.getCustomer(UUID);
        if (customer.isPresent()) {
            int unusedTokens = getTokensOfCustomer(customer.get())
                    .stream()
                    .filter(t -> !t.isUsed())
                    .collect(Collectors.toList())
                    .size();

            if (unusedTokens > 1) {
                return false;
            } else {
                int allowedTokens = Math.min(TOKEN_MAX - unusedTokens, n);
                for (int i = 0; i < allowedTokens; i++) {
                    dbAdapter.createToken(customer.get());
                }
                return true;
            }
        } else {
            return false;
        }
    }

}
