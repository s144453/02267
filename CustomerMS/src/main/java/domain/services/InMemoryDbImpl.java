package domain.services;

import domain.entities.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * @author Kasper
 */

public class InMemoryDbImpl implements DbAdapter {

    private static final InMemoryDbImpl INSTANCE = new InMemoryDbImpl();
    private static List<Token> tokenList;
    private static List<Customer> customerList;
    private static List<Payment> paymentList;


    private InMemoryDbImpl() {
        tokenList = new ArrayList<>();
        customerList = new ArrayList<>();
        paymentList = new ArrayList<>();
    }

    public static InMemoryDbImpl getInstance() {
        return INSTANCE;
    }


    @Override
    public Optional<Customer> getCustomer(String id) {
        return customerList.stream().filter(c -> c.getUniqueId().equals(id)).findFirst();
    }

    @Override
    public Optional<Token> getToken(String tokenId) {
        return tokenList.stream().filter(t -> t.getTokenId().equals(tokenId)).findFirst();
    }

    @Override
    public List<Token> getTokens() {
        return tokenList;
    }

    @Override
    public List<Payment> getPayments() {
        return paymentList;
    }

    @Override
    public Customer createCustomer(String firstName, String lastName) {
        Customer c = new Customer(firstName, lastName);
        customerList.add(c);
        return c;
    }

    @Override
    public void createToken(Customer customer) {
        Token t = new Token();
        t.setCustomer(customer);
        tokenList.add(t);
    }

    @Override
    public Payment createPayment(BigDecimal amount, Token token, Customer customer, String merchantId) {
        Payment p = new Payment(amount, token, customer, merchantId);
        paymentList.add(p);
        return p;
    }

    @Override
    public void updateName(Customer customer, String newFirstName, String newLastName) {
        customer.setFirstName(newFirstName);
        customer.setLastName(newLastName);
    }

}
