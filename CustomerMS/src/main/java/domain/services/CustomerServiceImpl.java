package domain.services;

import domain.entities.*;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author Jesper
 */

public class CustomerServiceImpl implements CustomerService {

    private static DbAdapter dbAdapter = InMemoryDbImpl.getInstance();
    private static BankService bank = new BankServiceService().getBankServicePort();


    @Override
    public String createCustomer(String firstName, String lastName, BigDecimal balance) {
        String customer = dbAdapter.createCustomer(firstName, lastName).getUniqueId();
        try {
            bank.createAccountWithBalance(new User(customer, firstName, lastName), balance);
        } catch (BankServiceException_Exception e) {
            return e.getCause().getMessage();
        }
        return customer;
    }

    @Override
    public List<Report> getListOfReports(String customerId) {
        List<Report> reportList = generateCustomerReports(customerId);
        reportList.sort(Comparator.comparing(Report::getReportId));

        return reportList;
    }

    @Override
    public String getReport(String cId, String rId) {
        return generateCustomerReports(cId).stream().filter(r -> r.getReportId().equals(rId)).findFirst().toString();
    }

    @Override
    public String changeCustomerName(String customerId, String newFirstName, String newLastName) {
        Optional<Customer> customer = dbAdapter.getCustomer(customerId);
        if (!customer.isPresent()) return "false";

        dbAdapter.updateName(customer.get(), newFirstName, newLastName);

        return customer.get().getName();
    }

    @Override
    public boolean deleteCustomer(String customerId) throws BankServiceException_Exception {
        if (!dbAdapter.getCustomer(customerId).isPresent()) return false;
        deleteBankAccount(customerId);
        return true;
    }

    @Override
    public boolean createCustomerPayment(BigDecimal amount, String tokenId, String merchantId, LocalDateTime time) {
        if(dbAdapter.getPayments().stream().anyMatch(p -> p.getToken().getTokenId().equals(tokenId))) return true;

        Optional<Token> token = dbAdapter.getToken(tokenId);
        if(!token.isPresent()) return false;

        Customer customer = token.get().getCustomer();
        Payment p = dbAdapter.createPayment(amount, token.get(), customer, merchantId);
        p.setTime(time);
        return true;
    }

    @Override
    public void refundCustomerPayment(String tokenId) {
        Optional<Payment> payment = dbAdapter.getPayments()
                .stream()
                .filter(p -> p.getToken().getTokenId().equals(tokenId)).findFirst();

        payment.ifPresent(p -> p.setRefund(true));

    }

    private void deleteBankAccount(String userId) throws BankServiceException_Exception {
        BankService bankService = getBankService();
        bankService.retireAccount(bankService.getAccountByCprNumber(userId).getId());
    }

    private BankService getBankService() {
        BankServiceService bank = new BankServiceService();
        return bank.getBankServicePort();
    }


    private List<Report> generateCustomerReports(String customerId) {
        // Extract relevant payments and group by date
        List<List<Payment>> payments = dbAdapter
                .getPayments()
                .stream()
                .filter(p -> p.getCustomer().getUniqueId().equals(customerId)).collect(Collectors.toList())
                .stream()
                .collect(Collectors.collectingAndThen(Collectors.groupingBy(Payment::getDate, Collectors.toList()), m -> new ArrayList<>(m.values())));

        List<Report> customerReports = new ArrayList<>();
        for(List<Payment> payGroups : payments) {
            List<ReportEntry> reportEntries = new ArrayList<>();
            for(Payment p : payGroups) {
                reportEntries.add(new CustomerReportEntry(p));
            }
            customerReports.add(new Report(reportEntries));
        }
        return customerReports;
    }
}
