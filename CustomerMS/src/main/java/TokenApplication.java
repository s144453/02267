
import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * @author Kasper
 */

@ApplicationPath("/")
public class TokenApplication extends Application {
}
