package controllers;

import MessageServices.TokenMessageService;
import domain.entities.Token;
import domain.services.TokenService;
import domain.services.TokenServiceImpl;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Sammy Saidpour Masoule
 */

@Path("/")
public class TokenController {

    private static TokenService tokenService = new TokenServiceImpl();
    private static TokenMessageService tokenMessageService = TokenMessageService.getInstance();


    @GET
    @Path("token/barcode")
    @Produces("image/png")
    public Response showBarcode(@QueryParam("tokenId") String tokenId) {
        return Response.ok(tokenService.getTokenImage(tokenId)).build();
    }

    @POST
    @Path("tokens/{n}")
    @Consumes(MediaType.TEXT_PLAIN)
    public Response requestTokens(@PathParam("n") int value, String UUID) {
        if (value > 5) {
            return Response.status(Response.Status.FORBIDDEN).entity("Too many tokens requested.").build();
        }
        List<Token> beforeTokens;

        beforeTokens = tokenService.getUnusedTokensByID(UUID);

        if (beforeTokens == null){
            return Response.status(Response.Status.NOT_FOUND).entity("No user with this ID").build();
        }
        if (tokenService.requestTokenByID(UUID, value)) {
            List<Token> newTokens = tokenService.getUnusedTokensByID(UUID);
            newTokens.removeAll(beforeTokens);

            List<String> urlList = new ArrayList<>();
            for (Token t : newTokens) {
                String s = "/tokens/barcode?tokenId=" + t.getTokenId();
                urlList.add(s);
            }


            String output = "{"
                    + "\"tokens\": \"" + newTokens.stream().map(Token::getTokenId).collect(Collectors.toList()) + "\","
                    + "\"BarcodeURL\": \"" + urlList + "\""
                    + "}";
            return Response.status(Response.Status.CREATED).entity(output).build();
        } else if (!tokenService.requestTokenByID(UUID, value)) {
            return Response.status(Response.Status.FORBIDDEN).entity("You have too many tokens.").build();
        }
        return Response.status(Response.Status.NOT_FOUND).entity("No user with this ID").build();
    }


    @POST
    @Path("token/{tid}/used")
    @Produces({"image/png", MediaType.TEXT_PLAIN})
    @Consumes(MediaType.TEXT_PLAIN)
    public Response useToken(@PathParam("tid") String tokenId) {
        try {
            if (tokenService.useToken(tokenId)) {
                return Response.ok().entity(tokenService.getTokenImage(tokenId)).build();
            } else if (!tokenService.useToken(tokenId)) {
                return Response.status(Response.Status.FORBIDDEN).entity("Token already used or wrong token id").build();
            }
        } catch (Exception e) {
            return Response.status(Response.Status.NOT_FOUND).entity("Token not found").build();
        }
        return null;
    }
}