package controllers;

import MessageServices.CustomerMessageService;
import domain.entities.Report;
import domain.services.CustomerService;
import domain.services.CustomerServiceImpl;
import dtu.ws.fastmoney.BankServiceException_Exception;
import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.GsonBuilder;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Jesper
 */

@Path("/customer")
public class CustomerController {

    private static CustomerService customerservice = new CustomerServiceImpl();
    private static CustomerMessageService customerMessageService = CustomerMessageService.getInstance();

    @POST
    @Path("/")
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createCustomer(String input) {
        JSONObject data = new JSONObject(input);
        String id;
        try {
            id = customerservice.createCustomer(
                    data.getString("firstName"),
                    data.getString("lastName"),
                    data.getBigDecimal("balance"));
        } catch (Exception e){
            return Response.status(Response.Status.BAD_REQUEST).entity(e.getStackTrace().toString()).build();
        }

        if (id.isEmpty()) return Response.status(Response.Status.BAD_REQUEST).build();
        else return Response.status(Response.Status.CREATED).entity(id).build();
    }

    @PUT
    @Path("/{cid}/name")
    @Produces({MediaType.TEXT_PLAIN, MediaType.APPLICATION_JSON})
    @Consumes(MediaType.APPLICATION_JSON)
    public Response changeCustomername(@PathParam("cid") String cid, String input) {
        JSONObject data = new JSONObject(input);
        String changeName;
        try {
            changeName = customerservice.changeCustomerName(
                    cid,
                    data.getString("newFirstName"),
                    data.getString("newLastName")
            );
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.toString()).build();
        }
        if (changeName.equals(data.getString("newFirstName")+" "+data.getString("newLastName"))) {
            return Response.ok(changeName).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/{cid}/report/{rid}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getReport(@PathParam("cid") String customerId, @PathParam("rid") String reportId){
        String report;
        try {
            report = customerservice.getReport(customerId, reportId);
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.toString()).build();
        }
        if (report.isEmpty()) { return Response.status(Response.Status.BAD_REQUEST).entity(report).build(); }
        else return Response.ok(report).build();
    }

    @GET
    @Path("/{cid}/reports")
    @Produces(MediaType.TEXT_PLAIN)
    public Response getReports(@PathParam("cid") String cid) {
        List<String> reports;
        try {
            reports = customerservice.getListOfReports(cid).stream()
                    .map(Report::getReportId)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.toString()).build();
        }

        if (reports.size() > 0) {
            Gson gson = new GsonBuilder().create();
            return Response.ok(gson.toJson(reports)).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @DELETE
    @Path("/{cid}")
    @Produces(MediaType.TEXT_PLAIN)
    public Response deleteCustomer(@PathParam("cid") String customerId) {
        try {
            if (customerservice.deleteCustomer(customerId)) { return Response.ok().build(); }
            else { return Response.status(Response.Status.BAD_REQUEST).build(); }
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Bank deletion error")
                    .build();
        }
    }

}
