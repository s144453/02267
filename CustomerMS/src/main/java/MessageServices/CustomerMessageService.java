package MessageServices;

import MessageClient.MessageClient;
import MessageClient.MessageQueueConstants;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import domain.services.CustomerService;
import domain.services.CustomerServiceImpl;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.concurrent.TimeoutException;

/**
 * @author Mads
 */

public class CustomerMessageService {
    private static CustomerService customerService;
    private static MessageClient messageClient;
    private static CustomerMessageService INSTANCE = new CustomerMessageService();

    public static CustomerMessageService getInstance() {
        return INSTANCE;
    }

    public CustomerMessageService() {
        CustomerMessageService.customerService = new CustomerServiceImpl();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(MessageQueueConstants.HostName);
        factory.setPort(MessageQueueConstants.HostPort);
        factory.setUsername(MessageQueueConstants.UserName);
        factory.setPassword(MessageQueueConstants.UserPassword);
        Connection connection;
        try {
            connection = factory.newConnection();

            Channel channel = connection.createChannel();

            messageClient = new MessageClient();

            //Parameters String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
            channel.queueDeclare(MessageQueueConstants.CustomerQueueName, false, false, false, null);

            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
                System.out.println(" [X] Received '" + message + "'");

                JSONObject jsonObj = new JSONObject(message);
                if (jsonObj.has(MessageQueueConstants.JSON_Target) && jsonObj.has(MessageQueueConstants.JSON_Content)) {
                    String target = jsonObj.getString(MessageQueueConstants.JSON_Target);
                    JSONObject content = jsonObj.getJSONObject(MessageQueueConstants.JSON_Content);

                    if (target.equals(MessageQueueConstants.Target_CustomerPayment)) {
                        handleMerchantMessage(content, target);
                    }
                }
            };

            channel.basicConsume(MessageQueueConstants.CustomerQueueName, true, deliverCallback, consumerTag -> {

            });
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

    private static void handleMerchantMessage(JSONObject content, String target) {
        String message = createMessage(content, target);
        System.out.println("ABOUT TO SEND THIS BACK: " + message);

        try {
            messageClient.put(MessageQueueConstants.MerchantQueueName, message);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String createMessage(JSONObject content, String target) {
        if (content.has(MessageQueueConstants.JSON_MerchantId)) {
            String merchant = content.getString(MessageQueueConstants.JSON_MerchantId);
            String token = content.getString(MessageQueueConstants.JSON_TokenId);
            BigDecimal amount = content.getBigDecimal(MessageQueueConstants.JSON_Amount);
            LocalDateTime time = LocalDateTime.parse(content.getString(MessageQueueConstants.JSON_DateTime));
            boolean wasRefunded = content.getBoolean(MessageQueueConstants.JSON_WasRefunded);

            if (customerService.createCustomerPayment(amount, token, merchant, time)) {
                if (wasRefunded) customerService.refundCustomerPayment(token);
            } else {
                String error = "JSONObject " + MessageQueueConstants.JSON_Content + " contained invalid " + MessageQueueConstants.JSON_TokenId;
                content.put(MessageQueueConstants.JSON_ErrorMessage, error);
            }
        } else {
            String error = "JSONObject " + MessageQueueConstants.JSON_Content + " did not contain key of name " + MessageQueueConstants.JSON_MerchantId;
            content.put(MessageQueueConstants.JSON_ErrorMessage, error);
        }

        JSONObject msg = new JSONObject();
        msg.put(MessageQueueConstants.JSON_Target, target);
        msg.put(MessageQueueConstants.JSON_Content, content);
        return msg.toString();
    }
}
