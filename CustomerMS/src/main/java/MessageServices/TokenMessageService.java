package MessageServices;

import MessageClient.MessageClient;
import MessageClient.MessageQueueConstants;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import domain.entities.Token;
import domain.services.TokenService;
import domain.services.TokenServiceImpl;
import org.json.JSONObject;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

/**
 * @author Mads
 */

public class TokenMessageService {

    private static TokenService tokenService;
    private static MessageClient messageClient;
    private static TokenMessageService INSTANCE = new TokenMessageService();

    public static TokenMessageService getInstance() {
        return INSTANCE;
    }

    public TokenMessageService() {
        TokenMessageService.tokenService = new TokenServiceImpl();
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(MessageQueueConstants.HostName);
        factory.setPort(MessageQueueConstants.HostPort);
        factory.setUsername(MessageQueueConstants.UserName);
        factory.setPassword(MessageQueueConstants.UserPassword);
        Connection connection;
        try {
            connection = factory.newConnection();

            Channel channel = connection.createChannel();

            messageClient = new MessageClient();

            //Parameters String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
            channel.queueDeclare(MessageQueueConstants.TokenQueueName, false, false, false, null);

            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), StandardCharsets.UTF_8);
                System.out.println(" [X] Received '" + message + "'");
                JSONObject jsonObj = new JSONObject(message);
                if(jsonObj.has(MessageQueueConstants.JSON_Target) && jsonObj.has(MessageQueueConstants.JSON_Content)) {

                    String target = jsonObj.getString(MessageQueueConstants.JSON_Target);
                    JSONObject content = jsonObj.getJSONObject(MessageQueueConstants.JSON_Content);

                    if(target.equals(MessageQueueConstants.Target_DebitCustomer) || target.equals(MessageQueueConstants.Target_RefundCustomer)) {
                        handleMerchantMessage(content, target);
                    }
                }
            };

            channel.basicConsume(MessageQueueConstants.TokenQueueName, true, deliverCallback, consumerTag -> {

            });
        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }


    private static void handleMerchantMessage(JSONObject content, String target) {
        String message = createMessage(content, target);

        try {
            messageClient.put(MessageQueueConstants.MerchantQueueName,message);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static String createMessage(JSONObject content, String target) {
        Optional<Token> optToken = tokenService.getTokenById(content.getString(MessageQueueConstants.JSON_TokenId));
        if(content.has(MessageQueueConstants.JSON_TokenId) && optToken.isPresent()) {
            Token token = optToken.get();

            String customerId = token.getCustomer().getUniqueId();

            content.put(MessageQueueConstants.JSON_CustomerId, customerId);

            if(target.equals(MessageQueueConstants.Target_DebitCustomer)) {
                content.put(MessageQueueConstants.JSON_WasUsed, token.isUsed());
                if(!token.isUsed()) {
                    token.use();
                }
            }
        } else {
            String error = "JSONObject " + MessageQueueConstants.JSON_Content + " did not contain key of name " + MessageQueueConstants.JSON_TokenId;
            content.put(MessageQueueConstants.JSON_ErrorMessage, error);
        }

        JSONObject msg = new JSONObject();
        msg.put(MessageQueueConstants.JSON_Target, target);
        msg.put(MessageQueueConstants.JSON_Content, content);
        return msg.toString();
    }


}
