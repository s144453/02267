package domain.entities;

import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * @author Dimitri
 */

public abstract class ReportEntry {
    private BigDecimal amount;
    private String tokenId;
    private String description;
    private LocalDateTime time;

    public ReportEntry(Payment payment) {
        this.amount = payment.getAmount();
        this.tokenId = payment.getTokenId();
        this.description = payment.getDescription();
        this.time = payment.getDateTime();
    }

    public BigDecimal getAmount() { return amount; }

    public String getTokenId() { return tokenId; }

    public String getDescription() { return description; }

    public LocalDateTime getDateTime() { return time; }
}
