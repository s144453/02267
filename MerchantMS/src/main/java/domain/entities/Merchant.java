package domain.entities;

import java.util.UUID;

/**
 * @author Kasper
 */

public class Merchant {

    private String uniqueId;
    private String firstName;
    private String lastName;

    public Merchant(String firstName, String lastName){
        this.uniqueId = UUID.randomUUID().toString();
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getUniqueId() {
        return uniqueId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

}
