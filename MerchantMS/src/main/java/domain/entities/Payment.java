package domain.entities;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.UUID;

/**
 * @author Kasper
 */

public class Payment {

    private String paymentId;
    private BigDecimal amount;
    private String tokenId;
    private Merchant merchant;
    private LocalDateTime time;
    private boolean refunded;
    private String description;

    public Payment(BigDecimal amount, String tokenId, Merchant merchant) {
        this.paymentId = UUID.randomUUID().toString();
        this.amount = amount;
        this.tokenId = tokenId;
        this.merchant = merchant;
        this.time = LocalDateTime.now();
        this.refunded = false;
        this.description = "";
    }

    public String getPaymentId() {
        return paymentId;
    }

    public void refund() {
        this.refunded = true;
    }

    public boolean isRefunded() {
        return refunded;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Merchant getMerchant() {
        return this.merchant;
    }

    public String getTokenId() {
        return tokenId;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public String getDescription() {
        return description;
    }

    public LocalDateTime getDateTime() { return this.time; }

    public LocalDateTime getDate() { return this.time.truncatedTo(ChronoUnit.DAYS); }
}
