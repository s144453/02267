package domain.services;

import domain.entities.Report;
import dtu.ws.fastmoney.BankServiceException_Exception;

import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeoutException;

/**
 * @author Kasper
 */

public interface MerchantService {

    boolean refundPayment(String merchantId, String paymentId) throws BankServiceException_Exception, TimeoutException, NotFoundException;

    Optional<String> debitCustomer(String mId, String tId, BigDecimal amount, String description) throws BankServiceException_Exception, TimeoutException;

    String createMerchant(String firstName, String lastName, BigDecimal amount) throws BankServiceException_Exception;

    List<Report> getListOfReports(String merchantId);

    String getReport(String mId, String rId);

    boolean changeMerchantName(String merchantId, String newFirstName, String newLastName);

    boolean deleteMerchant(String merchantId) throws BankServiceException_Exception;

}
