package domain.services;

import domain.entities.Merchant;
import domain.entities.Payment;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

/**
 * @author Kasper
 */

public interface DbAdapter {

    Optional<Merchant> getMerchant(String id);

    Optional<Payment> getPayment(String id);

    List<Payment> getPayments();

    Merchant createMerchant(String firstName, String lastName);

    Payment createPayment(BigDecimal amount, String tokenId, Merchant merchant);

    void refundPayment(Payment payment);

    void updateName(Merchant merchant, String newFirstName, String newLastName);


}
