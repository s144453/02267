package domain.services;

import MessageClient.MessageQueueConstants;
import MessageService.MerchantMessageService;
import domain.entities.*;
import dtu.ws.fastmoney.BankService;
import dtu.ws.fastmoney.BankServiceException_Exception;
import dtu.ws.fastmoney.BankServiceService;
import dtu.ws.fastmoney.User;
import org.json.JSONObject;

import javax.ws.rs.NotFoundException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

/**
 * @author Kasper
 * @co-author Mads
 */

public class MerchantServiceImpl implements MerchantService {

    private static DbAdapter dbAdapter = InMemoryDbImpl.getInstance();
    private static final int SECONDS_TO_WAIT = 10;

    @Override
    public boolean refundPayment(String merchantId, String paymentId) throws BankServiceException_Exception, TimeoutException, NotFoundException {
        Optional<Payment> paymentOpt = dbAdapter.getPayment(paymentId);
        if (!paymentOpt.isPresent()) throw new NotFoundException();
        if (paymentOpt.get().isRefunded() || !paymentOpt.get().getMerchant().getUniqueId().equals(merchantId)) {
            return false;
        }

        Payment payment = paymentOpt.get();

        MerchantMessageService.refundFuture = new CompletableFuture<>();
        putMessageInQueue(createTokenMessageContent(payment.getTokenId()),
                MessageQueueConstants.TokenQueueName,
                MessageQueueConstants.Target_RefundCustomer);

        try {
            System.out.println("WATING FOR ANSWER OF REFUND");
            String refundedCustomer = MerchantMessageService.refundFuture.get(SECONDS_TO_WAIT, TimeUnit.SECONDS);
            if (!refundedCustomer.isEmpty()) {
                transferMoneyFromTo(payment.getMerchant().getUniqueId(), refundedCustomer, payment.getAmount(), "REFUND: " + payment.getDescription());
                dbAdapter.refundPayment(payment);
                payment.refund();
                // todo: if this fails, customer gets no refund history
                sendPaymentInfoToCustomer(payment);
                return true;
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return false;
    }

    @Override
    public Optional<String> debitCustomer(String mId, String tId, BigDecimal amount, String description) throws BankServiceException_Exception, TimeoutException {
        Optional<Merchant> merchant = dbAdapter.getMerchant(mId);
        if (!merchant.isPresent()) return Optional.empty();

        MerchantMessageService.debitFuture = new CompletableFuture<>();
        putMessageInQueue(createTokenMessageContent(tId),
                MessageQueueConstants.TokenQueueName,
                MessageQueueConstants.Target_DebitCustomer);

        try {
            String debitCustomer = MerchantMessageService.debitFuture.get(SECONDS_TO_WAIT, TimeUnit.SECONDS);
            if (!debitCustomer.isEmpty() && !MerchantMessageService.tokenWasUsed) {
                transferMoneyFromTo(debitCustomer, merchant.get().getUniqueId(), amount, description);

                Payment payment = dbAdapter.createPayment(amount, tId, merchant.get());
                payment.setDescription(description);

                // todo: if this fails customer gets no payment history
                sendPaymentInfoToCustomer(payment);

                return Optional.of(payment.getPaymentId());
            }
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        return Optional.empty();
    }

    @Override
    public String createMerchant(String firstName, String lastName, BigDecimal balance) throws BankServiceException_Exception {
        Merchant merchant = dbAdapter.createMerchant(firstName, lastName);

        getBankService().createAccountWithBalance(new User(merchant.getUniqueId(),
                merchant.getFirstName(), merchant.getLastName()), balance);

        return merchant.getUniqueId();
    }

    @Override
    public List<Report> getListOfReports(String merchantId) {
        List<Report> reportList = generateMerchantReports(merchantId);
        reportList.sort(Comparator.comparing(Report::getReportId));

        return reportList;
    }

    @Override
    public String getReport(String mId, String rId) {
        return generateMerchantReports(mId).stream().filter(r -> r.getReportId().equals(rId)).findFirst().toString();
    }

    @Override
    public boolean changeMerchantName(String merchantId, String newFirstName, String newLastName) {
        Optional<Merchant> merchant = dbAdapter.getMerchant(merchantId);
        if (!merchant.isPresent() || newFirstName.isEmpty() || newLastName.isEmpty()) return false;

        dbAdapter.updateName(merchant.get(), newFirstName, newLastName);
        return true;
    }

    @Override
    public boolean deleteMerchant(String merchantId) throws BankServiceException_Exception {
        if (!dbAdapter.getMerchant(merchantId).isPresent()) return false;
        deleteBankAccount(merchantId);
        return true;
    }


    private void deleteBankAccount(String userId) throws BankServiceException_Exception {
        BankService bankService = getBankService();
        bankService.retireAccount(bankService.getAccountByCprNumber(userId).getId());
    }

    private void putMessageInQueue(JSONObject content, String messageQueueName, String target) {
        JSONObject msg = new JSONObject().put(MessageQueueConstants.JSON_Content, content).put(MessageQueueConstants.JSON_Target, target);

        try {
            MerchantMessageService.messageClient.put(messageQueueName, msg.toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private JSONObject createReportMessageContent(Payment payment) {
        return new JSONObject()
                .put(MessageQueueConstants.JSON_MerchantId, payment.getMerchant().getUniqueId())
                .put(MessageQueueConstants.JSON_TokenId, payment.getTokenId())
                .put(MessageQueueConstants.JSON_WasRefunded, payment.isRefunded())
                .put(MessageQueueConstants.JSON_Amount, payment.getAmount())
                .put(MessageQueueConstants.JSON_DateTime, payment.getDateTime());
    }

    private JSONObject createTokenMessageContent(String tokenId) {
        return new JSONObject().put(MessageQueueConstants.JSON_TokenId, tokenId);
    }

    private void sendPaymentInfoToCustomer(Payment payment) throws InterruptedException, ExecutionException, TimeoutException {
        MerchantMessageService.custRepFuture = new CompletableFuture<>();
        putMessageInQueue(createReportMessageContent(payment),
                MessageQueueConstants.CustomerQueueName,
                MessageQueueConstants.Target_CustomerPayment);

        String entryConfirm = MerchantMessageService.custRepFuture.get(SECONDS_TO_WAIT, TimeUnit.SECONDS);
        if(!entryConfirm.isEmpty()) {
            System.out.println("CUSTOMER GOT PAYMENT HISTORY - YAY");
        }
    }

    private BankService getBankService() {
        BankServiceService bank = new BankServiceService();
        return bank.getBankServicePort();
    }

    private void transferMoneyFromTo(String sendingUserId, String receiveingUserId, BigDecimal amount, String description) throws BankServiceException_Exception {
        BankService bankService = getBankService();
        String accountFrom = bankService.getAccountByCprNumber(sendingUserId).getId();
        String accountTo = bankService.getAccountByCprNumber(receiveingUserId).getId();
        bankService.transferMoneyFromTo(accountFrom, accountTo, amount, description);
    }

    private List<Report> generateMerchantReports(String merchantId) {
        // Extract relevant payments and group by date
        List<List<Payment>> payments = dbAdapter
                .getPayments()
                .stream()
                .filter(p -> p.getMerchant().getUniqueId().equals(merchantId)).collect(Collectors.toList())
                .stream()
                .collect(Collectors.collectingAndThen(Collectors.groupingBy(Payment::getDate, Collectors.toList()), m -> new ArrayList<>(m.values())));

        List<Report> merchantReports = new ArrayList<>();
        for (List<Payment> payGroups : payments) {
            List<ReportEntry> reportEntries = new ArrayList<>();
            for (Payment p : payGroups) {
                reportEntries.add(new MerchantReportEntry(p));
            }
            merchantReports.add(new Report(reportEntries));
        }
        return merchantReports;
    }
}
