package domain.services;

import domain.entities.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * @author Kasper
 */

public class InMemoryDbImpl implements DbAdapter {

    private static final InMemoryDbImpl INSTANCE = new InMemoryDbImpl();
    private static List<Merchant> merchantList;
    private static List<Payment> paymentList;


    private InMemoryDbImpl() {
        merchantList = new ArrayList<>();
        paymentList = new ArrayList<>();
    }

    public static InMemoryDbImpl getInstance() {
        return INSTANCE;
    }

    @Override
    public Optional<Merchant> getMerchant(String id) {
        return merchantList.stream().filter(m -> m.getUniqueId().equals(id)).findFirst();
    }

    @Override
    public Optional<Payment> getPayment(String id) {
        return paymentList.stream().filter(p -> p.getPaymentId().equals(id)).findFirst();
    }

    @Override
    public List<Payment> getPayments() {
        return paymentList;
    }

    @Override
    public Merchant createMerchant(String firstName, String lastName) {
        Merchant m = new Merchant(firstName, lastName);
        merchantList.add(m);
        return m;
    }

    @Override
    public Payment createPayment(BigDecimal amount, String tokenId, Merchant merchant) {
        Payment p = new Payment(amount, tokenId, merchant);
        paymentList.add(p);
        return p;
    }

    @Override
    public void refundPayment(Payment payment) {
        payment.refund();
    }


    @Override
    public void updateName(Merchant merchant, String newFirstName, String newLastName) {
        merchant.setFirstName(newFirstName);
        merchant.setLastName(newLastName);
    }

}
