package controllers;

import MessageService.MerchantMessageService;
import domain.entities.Report;
import domain.services.MerchantService;
import domain.services.MerchantServiceImpl;
import dtu.ws.fastmoney.BankServiceException_Exception;
import gherkin.deps.com.google.gson.Gson;
import gherkin.deps.com.google.gson.GsonBuilder;
import org.json.JSONException;
import org.json.JSONObject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.TimeoutException;
import java.util.stream.Collectors;

/**
 * @author Kasper
 */

@Path("/merchant")
public class MerchantController {

    private static MerchantService merchantService = new MerchantServiceImpl();
    private static MerchantMessageService merchantMessageService = MerchantMessageService.getInstance();

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    public Response createMerchant(String input) {
        JSONObject data = new JSONObject(input);

        try {
            String id = merchantService.createMerchant(
                    data.getString("firstName"),
                    data.getString("lastName"),
                    data.getBigDecimal("balance")
            );
            return Response.status(Response.Status.CREATED).entity(id).build();
        } catch (JSONException e) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("firstName and lastName must both be provided")
                    .build();
        } catch (BankServiceException_Exception e) {
            return Response
                    .status(Response.Status.BAD_REQUEST)
                    .entity("Error creating a bank account for the user")
                    .build();
        }
    }

    @DELETE
    @Produces(MediaType.TEXT_PLAIN)
    @Path("/{mId}")
    public Response deleteMerchantAccount(@PathParam("mId") String merchantId) {
        try {
            if (merchantService.deleteMerchant(merchantId)) {
                return Response.ok().build();
            } else {
                return Response.status(Response.Status.BAD_REQUEST).build();
            }
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.BAD_REQUEST)
                    .entity("Bank deletion error")
                    .build();
        }
    }


    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{mId}/payment")
    public Response debitCustomer(String input, @PathParam("mId") String mId) {
        JSONObject data = new JSONObject(input);

        try {
            Optional<String> paymentId = merchantService.debitCustomer(
                    mId,
                    data.getString("tId"),
                    data.getBigDecimal("amount"),
                    data.getString("description"));
            if (paymentId.isPresent()) {
                return Response.ok(paymentId.get()).build();
            } else {
                return Response.status(Response.Status.NOT_FOUND).build();
            }
        } catch (BankServiceException_Exception e) {
            e.printStackTrace();
            return Response.status(Response.Status.FORBIDDEN).entity("Bank error").build();
        } catch (TimeoutException e) {
            return Response.status(Response.Status.REQUEST_TIMEOUT).entity("Timeout waiting for answer").build();
        }
    }


    @POST
    @Path("/{mId}/payment/{pId}")
    public Response refundPayment(@PathParam("mId") String mId, @PathParam("pId") String pId) {
        try {
            if (merchantService.refundPayment(mId, pId)) {
                return Response.ok("").build();
            } else {
                return Response.status(Response.Status.FORBIDDEN)
                        .entity("Either the payment has already been refunded, " +
                                "or does not belong to the specified merchant")
                        .build();
            }

        } catch (BankServiceException_Exception e) {
            return Response.status(Response.Status.FORBIDDEN).entity("Bank error").build();
        } catch (TimeoutException e) {
            return Response.status(Response.Status.REQUEST_TIMEOUT).entity("Timeout waiting for answer").build();
        } catch (NotFoundException e) {
            return Response.status(Response.Status.NOT_FOUND).build();
        }
    }


    @SuppressWarnings("Duplicates")
    @GET
    @Path("/{mId}/reports")
    public Response getListOfReports(@PathParam("mId") String mId) {
        List<String> reports;
        try {
            reports = merchantService.getListOfReports(mId).stream()
                    .map(Report::getReportId)
                    .collect(Collectors.toList());
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.toString()).build();
        }

        if (reports.size() > 0) {
            Gson gson = new GsonBuilder().create();
            return Response.ok(gson.toJson(reports)).build();
        } else {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    @GET
    @Path("/{mId}/report/{rId}")
    public Response getReport(@PathParam("mId") String mId, @PathParam("rId") String rId) {
        String report;
        try {
            report = merchantService.getReport(mId, rId);
        } catch (Exception e) {
            return Response.status(Response.Status.BAD_REQUEST).entity(e.toString()).build();
        }
        if (report.isEmpty()) {
            return Response.status(Response.Status.BAD_REQUEST).entity(report).build();
        } else return Response.ok(report).build();
    }

    @PUT
    @Consumes(MediaType.APPLICATION_JSON)
    @Path("/{mId}/name")
    public Response changeMerchantName(@PathParam("mId") String mId, String input) {
        JSONObject data = new JSONObject(input);

        try {
            if (merchantService.changeMerchantName(mId,
                    data.getString("firstName"),
                    data.getString("lastName"))) {
                return Response.ok("Merchant name changed").build();

            }
        } catch (Exception ignored) {
        }
        return Response.status(Response.Status.BAD_REQUEST).build();

    }

}
