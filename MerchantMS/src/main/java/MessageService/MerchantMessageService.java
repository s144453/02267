package MessageService;

import MessageClient.MessageClient;
import MessageClient.MessageQueueConstants;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.DeliverCallback;
import domain.services.MerchantService;
import domain.services.MerchantServiceImpl;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.TimeoutException;

/**
 * @author Mads
 * @co-author Kasper
 */

public class MerchantMessageService {

    public static boolean tokenWasUsed;
    public static CompletableFuture<String> debitFuture;
    public static CompletableFuture<String> refundFuture;
    public static CompletableFuture<String> custRepFuture;
    public static MessageClient messageClient = new MessageClient();

    private static MerchantMessageService INSTANCE = new MerchantMessageService();

    public static MerchantMessageService getInstance(){
        return INSTANCE;
    }

    private MerchantMessageService() {
        ConnectionFactory factory = new ConnectionFactory();
        factory.setHost(MessageQueueConstants.HostName);
        factory.setPort(MessageQueueConstants.HostPort);
        factory.setUsername(MessageQueueConstants.UserName);
        factory.setPassword(MessageQueueConstants.UserPassword);
        Connection connection;
        try {
            connection = factory.newConnection();
            Channel channel = connection.createChannel();

            //Parameters String queue, boolean durable, boolean exclusive, boolean autoDelete, Map<String, Object> arguments
            channel.queueDeclare(MessageQueueConstants.MerchantQueueName, false, false, false, null);

            System.out.println(" [*] Waiting for messages. To exit press CTRL+C");

            DeliverCallback deliverCallback = (consumerTag, delivery) -> {
                String message = new String(delivery.getBody(), "UTF-8");
                System.out.println(" [X] Received '" + message + "'");

                JSONObject jsonObj = new JSONObject(message);
                String target = jsonObj.getString(MessageQueueConstants.JSON_Target);
                JSONObject content = jsonObj.getJSONObject(MessageQueueConstants.JSON_Content);

                if(content.has(MessageQueueConstants.JSON_ErrorMessage)) {
                    System.out.println(content.getString(MessageQueueConstants.JSON_ErrorMessage));
                } else if (target.equals(MessageQueueConstants.Target_RefundCustomer)) {
                    if (content.has(MessageQueueConstants.JSON_CustomerId)) {
                        System.out.println("DOING SOME REFUNDING");
                        refundFuture.complete(content.getString("CustomerId"));
                    }
                } else if (target.equals(MessageQueueConstants.Target_DebitCustomer)) {
                    if (content.has(MessageQueueConstants.JSON_CustomerId) && content.has(MessageQueueConstants.JSON_WasUsed)) {
                        tokenWasUsed = content.getBoolean(MessageQueueConstants.JSON_WasUsed);
                        debitFuture.complete(content.getString("CustomerId"));
                    }
                } else if(target.equals(MessageQueueConstants.Target_CustomerPayment)) {
                    custRepFuture.complete("OK");
                }

            };

            channel.basicConsume(MessageQueueConstants.MerchantQueueName, true, deliverCallback, consumerTag -> { });

        } catch (IOException | TimeoutException e) {
            e.printStackTrace();
        }
    }

}
